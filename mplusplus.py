import sys

from docx import Document
from docx.shared import Inches
from docx.enum.section import WD_SECTION_START
from docx.enum.section import WD_ORIENTATION
import docx


import re
import urllib.request
from lxml import html
from PIL import Image


def url_loader(url):
    with urllib.request.urlopen(url) as req:
        page = req.read().decode()
        global html_document
        global last_url
        last_url = url
        html_document = html.fromstring(page)


def img_loader(url, img_save_path, relative_path=True):
    # full_url = last_url + url if relative_path else url
    full_url = wiki_url + url if relative_path else url
    print(full_url)
    urllib.request.urlretrieve(full_url, img_save_path)
    # print(img_save_path)
    return img_save_path


def elem_for_list(xpath):
    global html_document
    res = html_document.xpath(xpath)
    res1 = res[0].xpath('div[contains(@class, "thumbcaption")]')
    print(len(res1))
    print("text_content: %s" % res1[0].text_content())
    return res


def for_solver(cycle_str, for_lines):
    xpath_str = ""
    i = 1
    while cycle_str[i][-1] != ':' and i < len(cycle_str):
        xpath_str = xpath_str + ' ' + cycle_str[i]
        i = i + 1
    xpath_str = xpath_str + ' ' + cycle_str[i][0:-1]
    xpath_str.lstrip()

    for x, i in zip(elem_for_list(xpath_str), range(31)):
        for y in for_lines:
            command_solver(y, x)
        # if i == 2:
        #     break

    """for x in elem_for_list(xpath_str):
        for y in for_lines:
            command_solver(y, x)"""


def command_solver(comm, _x=None):
    global document
    global doc_name
    commands = comm.split()
    if len(commands) == 0:
        return
    if commands[0] == "[docx]":
        if commands[1] == '+':
            doc_name = commands[2]
            document = Document()
        elif commands[1] == '-':
            if doc_name != "":
                document.save(doc_name)
        elif commands[1] == "+A4":
            document.add_page_break()
        elif commands[1] == "LANDSCAPE":
            print(*document.sections)
            for x in document.sections:
                new_w, new_h =x.page_height, x.page_width
                # x.orientation = WD_ORIENTATION.LANDSCAPE
                x.page_width = new_w
                x.page_height = new_h

                print(x.orientation)
            # document.sections[-1].orientation = WD_ORIENTATION.LANDSCAPE
        elif commands[1] == "PORTRAIT":
            document.sections[-1].orientation = WD_ORIENTATION.PORTRAIT
        elif commands[1] == "HEADING":
            if len(commands) == 2 or commands[2] == '#':
                return
            elif commands[2][0] == "'":
                add_str = ""
                i = 2
                while i < len(commands) and commands[i][-1] != "'":
                    add_str = add_str + ' ' + commands[i]
                    i = i + 1
                add_str = add_str + ' ' + commands[i][0:-1]
                document.add_heading(add_str[2:])
        elif commands[1] == "+img":
            global img_cnt
            if commands[2] != "_":
                print('give the item with a picture!')
                return
            # img_path = img_loader(_x.get('src'), "img%i.jpg" % img_cnt)
            src = _x.xpath("img")
            img_path = img_loader(_x.xpath(".//img")[0].get('src').rsplit('/', 1)[0].replace("/thumb", ""), "img%i.jpg" % img_cnt)
            img_cnt += 1
            print(img_path)
            try:
                document.add_picture(img_path, height=Inches(3))
            except:
                print("((((((")
        elif commands[1] == "+p":
            if len(commands) == 2 or commands[2] == '#':
                document.add_paragraph()
            elif commands[2][0] == "'":
                add_str = ""
                i = 2
                while i < len(commands) and commands[i][-1] != "'":
                    add_str = add_str + ' ' + commands[i]
                    i = i + 1
                add_str = add_str + ' ' + commands[i][0:-1]
                document.add_paragraph(add_str[2:])
            elif commands[2][0] == "_" and commands[2][1] == ".":
                xp = commands[2][2:]
                xp = _x.xpath('div[contains(@class, %s)]' % commands[2][2:])
                document.add_paragraph(_x.xpath('.//div[contains(@class, %s)]' % commands[2][2:])[0].text_content())
    elif re.findall('^http[s]?://', commands[0]):
        url_loader(commands[0])
    elif commands[0] == "все":
        for_lines = []
        next_l = sys.stdin.readline().rstrip()
        while next_l[0:3] == "   ":
            for_lines.append(next_l[3:])
            next_l = sys.stdin.readline().rstrip()
        for_solver(commands, for_lines)
        command_solver(next_l)


global document
global html_document
global last_url
# global wiki_url

img_cnt = 0
doc_name = ""
wiki_url = 'https:'
print("Input command:")
cur_command = sys.stdin.readline().rstrip()
while cur_command != "close":
    command_solver(cur_command)
    cur_command = sys.stdin.readline().rstrip()
